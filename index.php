<!DOCTYPE html>
<html>
    <head>
        <title>Eyecust</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/parallax.js"></script>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/effet_parallax.css">
	<link rel="stylesheet" href="css/tendances_stylesheet.css">
        <link rel="stylesheet" href="css/blog_stylesheet.css">
        <link rel="stylesheet" href="css/footer_stylesheet.css">
        <link rel="stylesheet" href="css/menu_stylesheet.css">
	<link rel="stylesheet" href="css/avis_stylesheet.css">
    </head>
    <body>
        <!--On applique l'effet parallaxe -->
        <?php require "php/header.php";?> <!-- On appelle le header -->
        <section class="text">
	<div id="tendances-container">
		<div id="titre_tendances">
			<h2>Nos Modèles Tendances</h2>
			<hr style="border: 1px solid black; width:100%;"/>
		</div>
		<div id="tendances-pics">
			<div id="modele1" class="card">
				<img src="/image/Femmes/kurashi.jpg" class="card__img">
				<div class="card__text">
					<h3 class="card__title">Karashi</h3>
					<p class="card__body">89.00 €</p>
				</div>
			</div>
			<div id="modele2" class="card">
				<img src="/image/Femmes/cali.jpg" class="card__img">
				<div class="card__text">
					<h3 class="card__title">Constantine</h3>
					<p class="card__body">89.00 €</p>
				</div>
			</div>
			<div id="modele3" class="card">
				<img src="/image/Femmes/dubaï.jpg" class="card__img">
				<div class="card__text">
					<h3 class="card__title">Tokyo</h3>
					<p class="card__body">89.00 €</p>
				</div>
			</div>
			<div id="modele4" class="card">
				<img src="/image/Femmes/baltimore.jpg" class="card__img">
				<div class="card__text">
					<h3 class="card__title">Bogota</h3>
					<p class="card__body">C'est beaucoup trop cher</p>
				</div>
			</div>
		</div>
	</div>
	</section>
        <section data-parallax="scroll" data-image-src="image/Fond/photo1.jpg"></section>
        <section class="text">
		<div id="boxAvis">
			<div id="avisTitre">
				<h2>Ce que les clients en disent...</h2>
				<br>
				<hr style="border: 1px solid black; width:100%;"/>
			</div>
			<div id="avis">
				<div id="avis1">
					<img id="client1" src="/image/photos_avis/man.svg" width="75px" heigth="75px">
					<h4 class="client-name" id="client-name1">John Jones</h4>
					<div class="stars">
						<img src="/image/photos_avis/fivestar.png" width="100px" height="25px">
					</div>
					<div class="client-review">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in sem purus. Donec ut neque mattis, faucibus nisl non, volutpat nisi. Curabitur ut ullamcorper mi, congue bibendum turpis. In consectetur, ipsum in blandit suscipit, sapien arcu eleifend mi, nec efficitur magna sapien sit amet tortor. In eget vestibulum urna. Nullam et nibh non felis faucibus tempor. Duis lectus dolor, semper quis volutpat et, facilisis et mi. Sed lobortis nunc et lectus consequat, sagittis lobortis tellus volutpat. 
					</div>	
				</div>
				<div id="avis2">
					<img id="client2" src="/image/photos_avis/woman.svg" width="75px" heigth="75px">
					<h4 class="client-name" id="client-name2">Jane Doe</h4>
					<div class="stars">
						<img src="/image/photos_avis/fivestar.png" width="100px" height="25px">
					</div>
					<div class="client-review">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in sem purus. Donec ut neque mattis, faucibus nisl non, volutpat nisi. Curabitur ut ullamcorper mi, congue bibendum turpis. In consectetur, ipsum in blandit suscipit, sapien arcu eleifend mi, nec efficitur magna sapien sit amet tortor. In eget vestibulum urna. Nullam et nibh non felis faucibus tempor. Duis lectus dolor, semper quis volutpat et, facilisis et mi. Sed lobortis nunc et lectus consequat, sagittis lobortis tellus volutpat. 
					</div>
				</div>			
				<div id="avis3">
					<img id="client3" src="/image/photos_avis/man.svg" width=75px" heigth="75px">
					<h4 class="client-name" id="client-name3">James Smith</h4>
					<div class="stars">
						<img src="/image/photos_avis/fivestar.png" width="100px" height="25px">
					</div>
					<div class="client-review">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in sem purus. Donec ut neque mattis, faucibus nisl non, volutpat nisi. Curabitur ut ullamcorper mi, congue bibendum turpis. In consectetur, ipsum in blandit suscipit, sapien arcu eleifend mi, nec efficitur magna sapien sit amet tortor. In eget vestibulum urna. Nullam et nibh non felis faucibus tempor. Duis lectus dolor, semper quis volutpat et, facilisis et mi. Sed lobortis nunc et lectus consequat, sagittis lobortis tellus volutpat. 
					</div>
				</div>	
			</div>
			<div id="avis-plus">
				<div id="more">Voir plus</div>
				<div id="add">Ajouter un avis</div>			
			</div>
		</div>
	</section>
        <section data-parallax="scroll" data-image-src="image/Fond/photo2.jpg"></section>
        <!-- On ajoute la partie Blog-->
        <section class="text">
            <div id="blogTitre">
				<h2>Notre Blog</h2>
				<hr>
            </div>
	 <div id="bigdaddy">
            <div id="blog">
                <div class="box">
                    <div class="imgBx">
                        <img src="image/Blog/1.jpg">
                    </div>
                    <div class="content">
                        <h3>Shooting Eyecust</h3>
                        <p>Gros shooting photo à Eragny (95).
                        Un photographe de talent, des models archi stylés, une ambiance de dingue.
                        Forcément le résultat est lourd.
                        Un grand merci à notre photographe, Latif, de FilmmirProduction et à nos models:
                        Nelly, Fus, Mathéo, Morgan, Hilal et Josian.
                        Merci l'équipe.</p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBx">
                        <img src="image/Blog/2.jpg">
                    </div>
                    <div class="content">
                        <h3>Comment commander</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
                <div class="box">
                    <div class="imgBx">
                        <img src="image/Blog/3.jpg">
                    </div>
                    <div class="content">
                        <h3>Les 3 fontaines</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                </div>
            </div>
	</div>
        </section>
        <section data-parallax="scroll" data-image-src="image/Fond/photo3.jpg"></section>
            <?php require "php/footer.php";?> <!-- On appelle le footer-->
    </body>
</html>
